var spawn = require('child_process').spawn;
var async = require('async');
var tmp = require('tmp');
var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');
var extend = require('util')._extend;
var log = require('winston');

module.exports.get = function(options, cb)
{
	options.env =  options.env || {};

	var keys =  options.fn.envHelper.read('.env.required');

	log.debug('Read required keys:', keys);

	var existing = options.fn.envHelper.read('.env') ;

	keys = extend(keys, existing);

	for(var k in keys){
		options.env[k] = existing[k] || '';
	}

	return cb(null, options.env);
}

module.exports.set = function(options, cb)
{
	log.debug('Setting Configuration: ', options.env);
	options.fn.envHelper.write('.env', options.env);
	cb();
}
