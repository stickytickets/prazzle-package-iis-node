var path = require('path');
var fs = require('fs');
var extend = require('util')._extend;
var log = require('winston');

exports.package = function(options, cb) {
    prepareOptions(options, function(options) {
        require('./package.js')(options, cb);
    });
}

exports.configure = {};

exports.configure.get = function(options, cb) {

    log.level = options.log_level;

    require('./configure.js').get(prepareOptions(options), cb);
}

exports.configure.set = function(options, cb) {

    log.level = options.log_level;

    require('./configure.js').set(prepareOptions(options), cb);
}

exports.install = function(options, cb) {
    log.level = options.log_level;
    require('./install.js')(options, cb);
}

function packageFilepath(options) {
    return path.join(options.outputDir, packageFilename(options, options));
}

function packageFilename(options) {
    return [options.project, '-', options.version, '.zip'].join('');
}

function packageEnvFilepath(options) {
    return path.join(options.outputDir, [options.project, '-', options.version, '.env'].join(''));
}

function readPackageJson() {
    var projectJsonPath = path.resolve('package.json');

    if (!fs.existsSync(projectJsonPath)) throw "Can't find project.json. Looked here: " + projectJsonPath;

    return require(projectJsonPath);
}

function gitVersionNode(options, cb) {
    options.fn.gitVersion(options.packageJson.version, function(error, v) {
        if (error) return cb(error);

        options.version = v;
        options.packageJson.version = options.version;

        log.info('Git Version Calculated: %s', options.version);

        cb(null, options);
    });
}

function prepareOptions(options, cb) {
    options.packageJson = readPackageJson(),
        options.outputDir = options.outputDir || ['..', 'dist', options.project].join(path.sep);

    gitVersionNode(options, function(err, options) {
        options.packageFilepath = packageFilepath(options);
        options.packageFilename = packageFilename(options);
        options.packageEnvFilepath = packageEnvFilepath(options);
        cb(options);
    });
}
