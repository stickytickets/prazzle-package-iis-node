var async = require('async');
var fs = require('fs');
var path = require('path');
var log = require('winston');

module.exports = function(options, cb)
{
	log.info('Packing IIS-Node App To: %s', options.outputDir);

	//return if already exists.
	if(fs.existsSync(path.resolve(options.packageFilepath) ) )
		return cb(null, createResult(options));

	async.waterfall([
		async.apply(packageToTmpDir, options),
		updatePackageJsonInPublishDir,
		getRequiredEnvironmentKeys,
		zipDirectory
	],function(err){

		if(err) return cb(err);

		cb(null, createResult(options));
	});
}

function createResult(options){
	return  [
		{
			file: path.resolve(options.packageFilepath),
			metadata:
			{
				name: options.project,
				version: options.version,
				extension: path.extname(options.packageFilepath),
				packageType: 'iis-node',
				deployment: 'server',
				install: options.install,
				env: options.environmentKeys
			}
		}
	];
}

function packageToTmpDir(options, cb) {
	log.verbose('Copying to temporary directory...');

	options.fn.packageToTmpDir(options.tmpDir, function(err) { cb(err, options); })
}

function updatePackageJsonInPublishDir(options, cb) {

	options.packageJson['package-type'] = 'iis-node';

	fs.writeFileSync(path.join(options.tmpDir,'package.json'), JSON.stringify(options.packageJson));

	cb(null, options);
}

function getRequiredEnvironmentKeys(options, cb) {
	log.verbose('Reading environment keys...');

	options.environmentKeys = options.fn.envHelper.read('.env.required');

	cb(null, options);
}

function writeEnvironmentKeys(options, cb) {
	log.verbose('Writing environment keys...');

	var outputFilename = options.packageEnvFilepath;

	log.verbose(outputFilename);

	options.fn.envHelper.writeKeys(outputFilename, options.environmentKeys);

	cb(null, options);
}

function zipDirectory(options, cb) {
	var outputFilename = options.packageFilepath;

	if (fs.existsSync(outputFilename)) fs.unlinkSync(outputFilename);

	options.fn.zipHelper.zipDir(options.tmpDir, outputFilename, function(e, r) { cb(e, options) });
}
